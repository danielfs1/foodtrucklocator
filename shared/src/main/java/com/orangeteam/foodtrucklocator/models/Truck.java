package com.orangeteam.foodtrucklocator.models;

import javax.persistence.Id;
import java.io.File;
import java.net.URL;
import java.util.Date;

public class Truck {

    @Id
    private String id;

    private String name;

    private String foodType;

    private Coordinate location;

    private Date locationDate;

    private URL websiteUrl;

    private File menu;

    public Truck() {
    }

    public Truck(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(final String foodType) {
        this.foodType = foodType;
    }

    public Coordinate getLocation() {
        return location;
    }

    public void setLocation(final Coordinate location) {
        this.location = location;
    }

    public Date getLocationDate() {
        return locationDate;
    }

    public void setLocationDate(final Date locationDate) {
        this.locationDate = locationDate;
    }

    public URL getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(final URL websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public File getMenu() {
        return menu;
    }

    public void setMenu(final File menu) {
        this.menu = menu;
    }
}
