package foodtrucklocator.controllers;

import com.orangeteam.foodtrucklocator.models.Truck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import foodtrucklocator.repos.TestRepo;

import javax.validation.Valid;

/**
 * Created by danielmoore on 10/2/15.
 */
@RestController
@RequestMapping("/")
public class TruckController {

    private static final String WELCOME_STRING = "Food Truxxxxx"
                                                 + "<br><br>" +
                                                 "Welcome to the Food Truck Locator";

    @Autowired
    private TestRepo testRepo;

    @RequestMapping(method = RequestMethod.GET)
    public String home() {
        return WELCOME_STRING;
    }

    @RequestMapping(value="trucks", method= RequestMethod.POST)
    public Truck create(@RequestBody @Valid Truck truck) {
        return this.testRepo.save(truck);
    }

    @RequestMapping(value="trucks", method=RequestMethod.GET)
    public List<Truck> list() {
        return this.testRepo.findAll();
    }

    @RequestMapping(value="trucks/{id}", method=RequestMethod.GET)
    public Truck get(@PathVariable("id") long id) {
        return this.testRepo.findOne(String.valueOf(id));
    }

    @RequestMapping(value="trucks/{id}", method=RequestMethod.PUT)
    public Truck update(@PathVariable("id") long id, @RequestBody @Valid Truck truck) {
        return testRepo.save(truck);

    }

    @RequestMapping(value="trucks/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable("id") long id) {
        this.testRepo.delete(String.valueOf(id));
        return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
    }
}