package foodtrucklocator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodtrucklocatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodtrucklocatorApplication.class, args);
    }

}
