package foodtrucklocator.repos;

import com.orangeteam.foodtrucklocator.models.Truck;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TestRepo extends MongoRepository<Truck, String> {

    List<Truck> findById(@Param("id") String id);

    Truck findOne(@Param("id") String id);

    Truck save(Truck truck);

    void delete(@Param("id") String id);

}
